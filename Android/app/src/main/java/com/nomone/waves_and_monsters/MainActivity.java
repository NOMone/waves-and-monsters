package com.nomone.waves_and_monsters;

import android.view.Menu;
import android.view.MenuItem;

import com.nomone.nongl.NONGLNatives;
import com.nomone.nongl.NonglActivity;
 
public class MainActivity extends NonglActivity {

	public MainActivity() {
		super(R.layout.main, R.id.rootLayout);
	}

	@Override
	protected NonglApplicationConfig getConfig() {
		NonglApplicationConfig config = new NonglApplicationConfig(false, false, 16, 0);
		return config;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		/*
		switch (item.getItemId()) {
		case R.id.<id>:
			
			// Report to native code,
			Natives.nativeOnMenuItemSelected(Natives.<ITEM CONSTANT>);			
			break;
		}
		*/
		
		NONGLNatives.nativeOnMenuItemSelected(0);
		
		return true;
	}
}
