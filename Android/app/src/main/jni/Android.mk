
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := natives
#APP_MODULES := stlport_shared.so
APP_MODULES := gnustl_shared.so

LOCAL_CFLAGS := 
LOCAL_CPPFLAGS += -I../../../../ -std=c++1y -fexceptions -frtti -DNONGL_GAME_SERVICES=1 -DNONGL_FACEBOOK_SDK=1

SRC_FILES = \
	$(wildcard ../../../../Nongl/libktx/*.c) \
	$(wildcard ../../../../Nongl/MiniZ/*.c) \
	$(wildcard ../../../../Nongl/LodePng/*.cpp) \
	$(wildcard ../../../../Nongl/StbImage/*.cpp) \
	$(wildcard ../../../../Nongl/Tests/*.cpp) \
	$(wildcard ../../../../Nongl/Backends/Android/CPP/*.cpp) \
	$(wildcard ../../../../Nongl/Shaders/*.cpp) \
	$(wildcard ../../../../Nongl/Json/*.cpp) \
	$(wildcard ../../../../Nongl/*.cpp) \
	$(wildcard ../../../../Src/*.cpp) \

#Append ../ to all files. It seems that "LOCAL_PATH := $(call my-dir)" forces compiler to work inside "jni", while
#the make utility resides in the project root folder. 
TEMP = $(SRC_FILES:%.cpp=../%.cpp)
SOURCES = $(TEMP:%.c=../%.c)

LOCAL_SRC_FILES := $(SOURCES) 
      
LOCAL_LDLIBS := -lGLESv2 -ldl -llog -landroid

include $(BUILD_SHARED_LIBRARY)
