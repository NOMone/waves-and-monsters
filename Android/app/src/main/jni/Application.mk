#NDK_TOOLCHAIN_VERSION := 4.8
APP_CPPFLAGS += -std=c++1y -fexceptions -frtti

APP_ABI := armeabi-v7a
APP_PLATFORM := android-9
#APP_STL := stlport_shared
#APP_STL := c++_shared
APP_STL := gnustl_shared
  
#STLPORT_FORCE_REBUILD := true
