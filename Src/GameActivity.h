#pragma once

#include <Nongl/Activity.h>
#include <Nongl/Button.h>
#include <Nongl/ImageView.h>

#include <vector>


namespace Ngl {
    class RepeatedImageView;
    class StereoLayout;
}

namespace WAM {

    class Wave {
    public:
        float speed;
        float time=0;

        Ngl::RepeatedImageView *view;
        Wave(float depth);

        void update(float elapsedTimeMillis);
    };

    class MonsterImageView : public ImageView {
    public:
        bool touched=false;

        MonsterImageView(const Ngl::TextureRegion *textureRegion) : ImageView(textureRegion) {}
        bool onTouchEvent(const TouchEvent *event) {
            touched = true;
            return true;
        }
    };

    class Monster {
    public:
        MonsterImageView *view;
        float age, duration;
        Monster();

        bool update(float elapsedTimeMillis);
    };

    class GameActivity : public Activity, ButtonListener {

        struct AliveToken {};
        Ngl::shared_ptr<AliveToken> aliveToken{new AliveToken()};

        std::vector<Wave> waves;
        std::vector<Monster> monsters;
        float timeToSpawnMonster = 2000;
        int bagsCount=0;
        bool lose = false;
        bool win = false;
        bool won = false;
        bool lost = false;
        float elapsedTime;

        Ngl::StereoLayout *gameLayout;

        void spawnMonsters(float elapsedTimeMillis);
        void addTrashBag();

    public:

        static const char *activityName;
        GameActivity();

        void onPause(bool externalPause);
        void onResume(bool externalResume);
        void onShow();
        bool onBackPressed();

        void onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ);

        void buttonOnClick(Button *button);
    };
}
