#pragma once

#define VERBOSE true
#define CHECK_GL_ERRORS true

#define APPLICATION_TITLE "Waves and Monsters!"

#define TEXTURE_PACK "CombinedPack.txt"

#define DESIGN_WIDTH 1920.0f
#define DESIGN_HEIGHT 1080.0f
