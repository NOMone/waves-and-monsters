#include "TextActivity.h"

#include <Src/ResMgr.h>

#include <Nongl/Audio.h>

#include <Nongl/RepeatedImageView.h>
#include <Nongl/TextView.h>
#include <Nongl/SystemLog.h>
#include <Src/GameActivity.h>

using namespace WAM;
using namespace Ngl;

#define PAUSE_BUTTON_ID 1

const char *TextActivity::activityName = "Game activity";

/////////////////////////
// Game Activity
/////////////////////////

TextActivity::TextActivity(int state) : Activity(activityName) {

    this->state = state;

    // Construct GUI,
    if (state == 0) {
        rootLayout.addView(new TextView("Humans throw their trash into the oceans. The monsters of the deep decided to return them back. Under the full moon, they rise and throw trash bags on the land. It's up to you to save the world. When you see a monster, bonk him on the head. It's not like these garbage will always be there. It's a matter of a few centuries, millenniums max before they decompose!", 1920, 0.5, Gravity::CENTER));
    } else if (state == 1) {
        rootLayout.addView(new TextView("You lost, the land is now a huge trash can!", 1920, 0.75, Gravity::CENTER));
    } else {
        rootLayout.addView(new TextView("You still lost, the ocean is now a huge trash can!", 1920, 0.75, Gravity::CENTER));
    }

    Button *button = new Button(0, 0, "Play");
    button->setCenter(rootLayout.getWidth()*0.5f, rootLayout.getHeight()*0.5f);
    button->setListener(this);
    rootLayout.addView(button);

    rootLayout.layout();
}

void TextActivity::onPause(bool externalPause) {
    ResMgr::getBackgroundMusic()->pause();
}

void TextActivity::onResume(bool externalResume) {
    // TODO: check if music is not muted?
    ResMgr::getBackgroundMusic()->resume();
}

void TextActivity::onShow() {
    javaPreventSleep(true);

    // Play background music,
    if (javaGetBooleanPreference(ResMgr::MUSIC_ON_PREFERENCE, true)) {
        auto backgroundMusic = ResMgr::getBackgroundMusic();
        #ifdef EMSCRIPTEN
        backgroundMusic->setLooping(true);
        #else
        backgroundMusic->play();
        backgroundMusic->setLooping(true);
        #endif
    }
}

bool TextActivity::onBackPressed() {

    // Fade out the background music,
    ResMgr::getBackgroundMusic()->fadeOut(250);

    // Exit,
    //javaExit();
    //javaFinishActivity();
    finish();
    return true;
}

void TextActivity::buttonOnClick(Button *button) {

    finish();
    Nongl::startActivity(new WAM::GameActivity(), false);
}
