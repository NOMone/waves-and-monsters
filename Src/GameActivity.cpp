#include "GameActivity.h"

#include <Src/ResMgr.h>

#include <Nongl/Audio.h>

#include <Nongl/StereoLayout.h>
#include <Nongl/RepeatedImageView.h>
#include <Nongl/TextView.h>
#include <Nongl/SystemLog.h>
#include <Nongl/utils.h>

#include <Src/TextActivity.h>

using namespace WAM;
using namespace Ngl;

#define PAUSE_BUTTON_ID 1

#define WAVES_COUNT 20

const char *GameActivity::activityName = "Game activity";

/////////////////////////
// Wave
/////////////////////////

Wave::Wave(float depth) {
    view = new RepeatedImageView(ResMgr::wave, 8);
    view->setLeft(-1500);
    view->setBottom(0);
    view->setDepth(depth);

    speed = 0;
    while (absolute(speed) < 0.05) {
        speed = randomFloatRange(-0.3, 0.3);
    };
}

void Wave::update(float elapsedTimeMillis) {
    view->setLeft(view->getLeft() + speed*elapsedTimeMillis);
    while (view->getRight() >=  5*ResMgr::wave->originalWidth) view->setLeft(view->getLeft()-ResMgr::wave->originalWidth);
    while (view->getLeft () <= -5*ResMgr::wave->originalWidth) view->setLeft(view->getLeft()+ResMgr::wave->originalWidth);

    time += (speed*elapsedTimeMillis)/300;
    view->setBottom(-100 + 50*sinf(time));
}

/////////////////////////
// Monster
/////////////////////////

float effectiveWidth;

Monster::Monster() {

    view = new MonsterImageView(ResMgr::monster);
    float depth = randomFloatRange(3.0, 5);
    view->setDepth(depth);
    view->setLeft((-0.5f*effectiveWidth*depth) + randomFloatRange(0, effectiveWidth*depth));

    age = 0;
    duration = randomFloatRange(1000, 2000);
}

bool Monster::update(float elapsedTimeMillis) {
    age+=elapsedTimeMillis;

    float riseRatio = 4 * age / duration;
    if (riseRatio < 1) view->setBottom(-50 + view->getHeight()*(sinf(0.5f*PI*riseRatio) - 1));

    if (riseRatio > 3) {
        riseRatio = 4 - riseRatio;
        view->setBottom(-50 + view->getHeight()*(sinf(0.5f*PI*riseRatio) - 1));
    }

    if (view->touched) {
        if (age < 0.25f * duration) {
            age = duration - age;
        } else if (age < 0.75f * duration) {
            age = 0.75f * duration;
        }
    }

    if (age >= duration) return true;
    return false;
}

/////////////////////////
// Game Activity
/////////////////////////

GameActivity::GameActivity() : Activity(activityName) {

	// Construct GUI,
    rootLayout.addView(new TextView("besm Allah", 1920, 3, Gravity::CENTER));

    // Game layout,
    gameLayout = new StereoLayout(0, 0, rootLayout.getWidth(), rootLayout.getHeight());
    rootLayout.addView(gameLayout);

    // Background,
    ImageView *background = new ImageView(ResMgr::gameBackground);
    gameLayout->addView(background);

    ColorView *seaColor = new ColorView(0, 0, rootLayout.getWidth(), rootLayout.getHeight()*0.4f, 0xff74360c);
    gameLayout->addView(seaColor);

    // Add waves,
    for (int32_t i=WAVES_COUNT-1; i>-1; i--) {
        float depth = 1.5 + (8*i / (float)(WAVES_COUNT-1));
        waves.emplace_back(depth);
        Wave &wave = waves.back();
        gameLayout->addView(wave.view);
    }

    // TODO: stereo/mono (cardboard) button...

    rootLayout.layout();
}

void GameActivity::onPause(bool externalPause) {
    ResMgr::getBackgroundMusic()->pause();
}

void GameActivity::onResume(bool externalResume) {
    // TODO: check if music is not muted?
    ResMgr::getBackgroundMusic()->resume();
}

void GameActivity::onShow() {
    javaPreventSleep(true);

    // Play background music,
    if (javaGetBooleanPreference(ResMgr::MUSIC_ON_PREFERENCE, true)) {
        auto backgroundMusic = ResMgr::getBackgroundMusic();
        #ifdef EMSCRIPTEN
        backgroundMusic->setLooping(true);
        #else
        backgroundMusic->play();
        backgroundMusic->setLooping(true);
        #endif
    }
}

bool GameActivity::onBackPressed() {

    // Fade out the background music,
    ResMgr::getBackgroundMusic()->fadeOut(250);

    // Exit,
    //javaExit();
	//javaFinishActivity();
    finish();
	return true;
}

void GameActivity::spawnMonsters(float elapsedTimeMillis) {

    timeToSpawnMonster -= elapsedTimeMillis;
    if (timeToSpawnMonster <= 0) {
        timeToSpawnMonster = randomFloatRange(500, 2000);
        monsters.emplace_back();
        gameLayout->addView(monsters.back().view);
    }
}

void GameActivity::addTrashBag() {
    ImageView *view = new ImageView(ResMgr::trashBag);
    float scale = randomFloatRange(1, 3);
    view->setScale(scale, scale);
    view->setLeft(randomFloatRange(-effectiveWidth*0.5, effectiveWidth*0.5));
    view->setBottom(randomFloatRange(-gameLayout->getHeight()*0.5, gameLayout->getHeight()*0.5));
    view->setDepth(1.0);
    gameLayout->addView(view);

    bagsCount++;
    if (bagsCount == 10) lose = true;
}

void GameActivity::onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ) {

    elapsedTime += elapsedTimeMillis;
    if (elapsedTime >= 30000) win = true;

    if (win && !won) {
        //Activity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);
        finish();
        Nongl::scheduler.schedule([](void *)->bool {
            Nongl::startActivity(new WAM::TextActivity(2));
            return true;
        }, 600, 0, 0);
        won = true;
        return;
    } else if (lose && !lost) {
        //Activity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);
        finish();
        Nongl::scheduler.schedule([](void *)->bool {
            Nongl::startActivity(new WAM::TextActivity(1));
            return true;
        }, 600, 0, 0);
        lost = true;
        return;
    }

    // Update game state,
    // Move waves,
    for (int32_t i=0; i<waves.size(); i++) {
        waves[i].update(elapsedTimeMillis);
    }

    // Spawn monsters,
    effectiveWidth = gameLayout->getEffectiveWidth();
    spawnMonsters(elapsedTimeMillis);

    // Update monsters,
    for (int32_t i=monsters.size()-1; i>-1; i--) {
        if (monsters[i].update(elapsedTimeMillis)) {
            gameLayout->removeView(monsters[i].view);
            if (!monsters[i].view->touched) {
                addTrashBag();
            }

            monsters.erase(monsters.begin()+i);
        }
    }

    // Sort by depth,
    gameLayout->sort();

    // Draw,
    Activity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);
}

void GameActivity::buttonOnClick(Button *button) {

    switch (button->tag) {
        case PAUSE_BUTTON_ID:
            // TODO: ...
            break;
        default:
            break;
    }
}
