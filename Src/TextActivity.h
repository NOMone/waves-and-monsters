#pragma once

#include <Nongl/Activity.h>
#include <Nongl/Button.h>

namespace WAM {

    class TextActivity : public Activity, ButtonListener {

        struct AliveToken {};
        Ngl::shared_ptr<AliveToken> aliveToken{new AliveToken()};

    public:

        int state;
        static const char *activityName;
        TextActivity(int state);

        void onPause(bool externalPause);
        void onResume(bool externalResume);
        void onShow();
        bool onBackPressed();

        void buttonOnClick(Button *button);
    };
}
