#pragma once

#include <Nongl/ManagedObject.h>

#include <stdint.h>
#include <vector>

class NinePatchData;
class Music;

namespace Ngl {
    class TextureRegion;
}

namespace WAM {

    class ResMgr {
    public:

        static const char *MUSIC_ON_PREFERENCE;

        static const Ngl::TextureRegion *gameBackground;
        static const Ngl::TextureRegion *wave;
        static const Ngl::TextureRegion *monster;
        static const Ngl::TextureRegion *trashBag;

        static const NinePatchData *dialogBackground;
        static const NinePatchData *dialogBackground2;

        static const Ngl::TextureRegion *pauseButton;

        static void loadResources();

        static Ngl::shared_ptr<Music> getBackgroundMusic();
    };
}
