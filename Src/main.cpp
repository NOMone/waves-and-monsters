#include <Nongl/Nongl.h>
#include <Nongl/SystemLog.h>
#include <Nongl/AudioManager.h>
#include <Nongl/Button.h>
#include <Nongl/SystemInterface.h>

#include <Src/ResMgr.h>
#include <Src/GameActivity.h>
#include <Src/TextActivity.h>.h>

using namespace Ngl;
using namespace WAM;

void Nongl::main() {

	LOGE("besm Allah :)");

	ResMgr::loadResources();

	// Load and setup sound assets,
	Button::setClickSound(Nongl::audioManager->addSound("sounds/buttonClick", "buttonClick"));

    // Start activity,
    startActivity(new WAM::TextActivity(0), false);
}
