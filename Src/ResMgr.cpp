#include <Src/ResMgr.h>

#include <Nongl/DisplayManager.h>
#include <Nongl/NinePatchSprite.h>
#include <Nongl/Audio.h>

using namespace Ngl;
using namespace WAM;

const char *ResMgr::MUSIC_ON_PREFERENCE = "Music on";

const TextureRegion *ResMgr::gameBackground;
const TextureRegion *ResMgr::wave;
const TextureRegion *ResMgr::monster;
const TextureRegion *ResMgr::trashBag;

const NinePatchData *ResMgr::dialogBackground;
const NinePatchData *ResMgr::dialogBackground2;

const TextureRegion *ResMgr::pauseButton;

void ResMgr::loadResources() {

    // Set constants,
    // ... any constants?

    // Load textures,
    DisplayManager *displayManager = DisplayManager::getSingleton();

    gameBackground = displayManager->getTextureRegion("Game.backgroundWide");
    wave           = displayManager->getTextureRegion("Game.wave"          );
    monster        = displayManager->getTextureRegion("Game.monster1"      );
    trashBag       = displayManager->getTextureRegion("Game.trashBag"      );

    static NinePatchData dialogBackgroundNinePatchData;
    dialogBackgroundNinePatchData.textureRegion = displayManager->getTextureRegion("UI.dialogBackground");
    dialogBackgroundNinePatchData.setColumnWidths(3, false, 27, 3, 27);
    dialogBackgroundNinePatchData.setRowHeights  (3, false, 33, 3, 33);
    dialogBackground = &dialogBackgroundNinePatchData;

    static NinePatchData dialogBackground2NinePatchData;
    dialogBackground2NinePatchData.textureRegion = displayManager->getTextureRegion("UI.dialogBackground2");
    dialogBackground2NinePatchData.setColumnWidths(3, false, 23, 7, 23);
    dialogBackground2NinePatchData.setRowHeights  (3, false, 22, 3, 22);
    dialogBackground2 = &dialogBackground2NinePatchData;

    pauseButton = displayManager->getTextureRegion("UI.pauseButton");
}

Ngl::shared_ptr<Music> ResMgr::getBackgroundMusic() {

    static Ngl::shared_ptr<Music> music;
    if (music.expired()) music.manage(new Music("music/background", "Main menu music"));
    return music;
}
